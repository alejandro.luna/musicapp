import { IAction, IPlayListTop } from '../interface/interface';

const FAVORITES_ADD = '[FV ADD] FAVORITES ADD'
const FAVORITES_DELETE = '[FV DEL] FAVORITES DELETE'
const FAVORITES_CLEAN = '[FV CL] FAVORITES CLEAN'

const initialState = {
    favoritesPlayList: []
}

export const addFavoritesPlayList = ({id,name,description,images,isFavorite = true}: IPlayListTop) =>  (
    {
        type: FAVORITES_ADD, 
        payload: {
            id: id,
            name: name,
            isFavorite: isFavorite,
            description: description,
            images: images
        }
    }
)
export const favoritesClean = () => ({type: FAVORITES_CLEAN})
export const deleteFavoriteSong = (id: string) => ({
    type: FAVORITES_DELETE,
    payload: id
})

const favoritesReducer = (state = initialState, action: IAction) => {
    switch (action.type) {
        case FAVORITES_ADD:
            return {
                ...state,
                favoritesPlayList: [...state.favoritesPlayList, action.payload]
            }
        case FAVORITES_DELETE:
            return {
                ...state,
                favoritesPlayList: state.favoritesPlayList.filter((e: any) => e.id !== action.payload)
            }
        case FAVORITES_CLEAN:
            return []
        default:
            return state
    }
}

export default favoritesReducer