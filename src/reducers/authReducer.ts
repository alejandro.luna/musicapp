import { IAction } from '../interface/interface';

const AUTH_LOADING = '[AUTH LDG] AUTH LOADING'
const AUTH_LOADED = '[AUTH LD] AUTH LOADED'
const AUTH_LOGIN = '[AUTH LG] AUTH LOGIN'
const AUTH_LOGOUTH = '[AUTH LGH] AUTH LOGOUTH'
const AUTH_HASH_LOGIN = '[AUTH H L] AUTH HASH LOGIN'

const initialState = {
    isAuthenticated: false,
    user: {},
    access: {},
    loading: false
}

export const authLoading = () => ({ type: AUTH_LOADING })
export const authLoaded = () => ({ type: AUTH_LOADED })
export const authHash = (token: string, expire_in: string, token_type: string) => (
    {
        type: AUTH_HASH_LOGIN,
        payload: {
            token,
            expire_in,
            token_type
        }
    }
)
export const authLoginUser = (data: {}) => ({ type: AUTH_LOGIN, payload: data })
export const authClean = () => ({ type: AUTH_LOGOUTH })

const authReducer = (state = initialState, action: IAction) => {
    switch (action.type) {
        case AUTH_LOADING:
            return {
                ...state,
                loading: true
            }
        case AUTH_LOADED:
            return {
                ...state,
                loading: false
            }
        case AUTH_HASH_LOGIN:
            return {
                ...state,
                access: action.payload,
                isAuthenticated: true,
            }
        case AUTH_LOGIN:
            return {
                ...state,
                user: action.payload
            }
        case AUTH_LOGOUTH:
            return state === initialState
        default:
            return state
    }
}

export default authReducer