import { IAction, IPlayListTop } from '../interface/interface';

const FEATURE_LOADED = '[FT LDG] FT LOADING'
const FEATURE_CLEAN = '[FT CL] FT CLEAN'
const FEATURE_SET_ISFAVORITE = '[FT S IFV] FT SET ISFAVORITE'

const initialState = {items: []};

export const featureLoaded = (data: {}) => ({type: FEATURE_LOADED, payload: data})
export const featureClean = () => ({type: FEATURE_CLEAN})
export const featureSetIsFavorite = (id: string, isFavorite: boolean) => (
    {
        type: FEATURE_SET_ISFAVORITE, 
        payload: {id, isFavorite}
    }
)

const featurePlayListReducer = (state: any = initialState, action: IAction) => { 
    switch (action.type) {
        case FEATURE_LOADED:
            return {
                ...state,
                items: action.payload
            }
        case FEATURE_SET_ISFAVORITE:
            return {
                ...state,
                items: state.items.map(
                    (e: IPlayListTop) => (e.id === action.payload.id)
                    ? {...e, isFavorite: action.payload.isFavorite}
                    : e
                )
            }
        case FEATURE_CLEAN:
            return []
        default:
            return state
    }
}


export default featurePlayListReducer
