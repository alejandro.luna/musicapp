import {createStore,combineReducers,applyMiddleware,compose} from 'redux'
import thunk from 'redux-thunk'
import authReducer from '../reducers/authReducer';
import featurePlayListReducer from '../reducers/featurePlayListReducer';
import favoritesReducer from '../reducers/favoritesReducer';

declare global {
    interface Window {
        __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: typeof compose
    }
}

const composeEnhancers =(typeof window !== 'undefined' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) || compose;

const reducer = combineReducers({
    auth: authReducer,
    featurePlayList: featurePlayListReducer,
    favoritesPlayList: favoritesReducer,
})

export const store = createStore(
    reducer,
    composeEnhancers(applyMiddleware(thunk))
)