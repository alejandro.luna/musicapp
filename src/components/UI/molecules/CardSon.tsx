import SongImage from '../atoms/SongImage';
import SongName from '../atoms/SongName';
import SongElement from '../atoms/SongElement';
import { ISong } from '../../../interface/interface';
import SongDescription from '../atoms/SongDescription';



const CardSong = ({dataSog}: ISong) => {

  return (
    <div className='card__song'>
      <SongImage image={dataSog.images} />
      <SongName name={dataSog.name} />
      <SongDescription name={dataSog.description} />
      <SongElement dataSog={dataSog} />
    </div>
  )
}

export default CardSong