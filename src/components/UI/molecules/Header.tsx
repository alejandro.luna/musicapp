import Logo from '../atoms/Logo';
import Navigate from '../atoms/Navigate';
import ButtonLogout from '../atoms/ButtonLogout';
import { useSelector, RootStateOrAny } from 'react-redux';
import { routerPrivate } from '../../../routers/routers';


const Header = () => {
  const {body} =  useSelector((state: RootStateOrAny) => state.auth.user)

  return (
    <>
        <Logo classFigure='wrapper__nav__figure' classImage='wrapper__nav__img' />
        <h3 className='wrapper__nav__name'>{body?.display_name}</h3>
        <section className='wrapper__nav__menu'>
        {
          routerPrivate.filter(route => route.name !== '')
          .map((data, index) => (
            <Navigate 
              key={index}  
              icon={data.icon} to={data.to} value={data.name} 
            />
          ))
        }
        </section>
 
        <ButtonLogout text='Logout'/>
    </>
  )
}

export default Header