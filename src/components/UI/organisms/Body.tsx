import { IPropsChildren } from '../../../interface/interface';

const Body = ({children}: IPropsChildren) => <section className='wrapper__body'>{children}</section>

export default Body