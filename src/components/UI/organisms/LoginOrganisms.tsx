import ButtonLogin from '../atoms/ButtonLogin'
import Logo from '../atoms/Logo'
import { authorizeURL } from '../../../services/spotify';
import { ILogo } from '../../../interface/interface';

const LoginOrganisms = ({classFigure,classImage}:ILogo) => {
  return (
    <>
        <Logo classFigure={classFigure} classImage={classImage} />
        <ButtonLogin text='Login with spotify' link={authorizeURL} />
    </>
  )
}

export default LoginOrganisms