import Header from '../molecules/Header';

const Sidebar = () => {
  return (
    <nav className='wrapper__nav'>
      <Header />
    </nav>
  )
}

export default Sidebar