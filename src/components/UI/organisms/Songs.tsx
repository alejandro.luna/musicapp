import CardSong from '../molecules/CardSon';
import { ISongs } from '../../../interface/interface';


const Songs = ({dataSogs}: ISongs) => {
  return (
    <div className='wrapper__body_cards'>
      {
        dataSogs?.map((song: any, index) => (
          <CardSong 
            key={index} 
            dataSog={song}
          />
        ))
      }
    </div>
  )
}

export default Songs