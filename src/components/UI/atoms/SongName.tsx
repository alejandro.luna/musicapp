import { ILabel } from '../../../interface/interface';

const SongName = ({name}: ILabel) => {
  return (
    <h3 className='card__song_name'>{name}</h3>
  )
}

export default SongName