import { IImage } from '../../../interface/interface';


const SongImage = ({image}: IImage) => {
  return (
    <figure className='card__song_figure'>
      <img src={image[0].url} alt='Logo' className='card__song_img' />
    </figure>
  )
}

export default SongImage