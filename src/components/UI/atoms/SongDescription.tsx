import { ILabel } from '../../../interface/interface';

const SongDescription = ({name}: ILabel) => {
    return (
        <p className='card__song_description'>{name}</p>
    )
}

export default SongDescription