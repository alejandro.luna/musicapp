import { ILabel } from '../../../interface/interface';

const PageName = ({name}: ILabel ) => {
  return (
    <h2 className='page__name'>{name}</h2>
  )
}

export default PageName