import { ISong } from '../../../interface/interface';
import { useDispatch } from 'react-redux';
import { handleSetFavorite } from '../../../helpers/favorites';

const SongElement = ({dataSog}:ISong) =>  {
    const dispatch = useDispatch()
    return(
        <i 
            className={dataSog.isFavorite ? 'card__song_icon fa fa-star': 'card__song_icon fa fa-star-o'} 
            onClick={() => dispatch(handleSetFavorite({dataSog}))} 
        />
    )
}

export default SongElement