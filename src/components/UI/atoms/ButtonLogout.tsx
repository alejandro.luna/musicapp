import { IBtn } from '../../../interface/interface';
import { sessionExpired } from '../../../helpers/auth';
import { useDispatch } from 'react-redux';

const ButtonLogout = ({text}:IBtn) => {
  const dispatch = useDispatch();
  return (
    <section className='wrapper__nav__btn'>
      <button 
        type='button'  
        className='btn btn_logout btn--danger'
        onClick={() => dispatch(sessionExpired())} 
        >{text}</button>
    </section>
  )
}

export default ButtonLogout