import { INavigate } from '../../../interface/interface';
import { NavLink } from 'react-router-dom';

const Navigate = ({to, value, icon}: INavigate) => {
    return(
        <NavLink to={to} className='wrapper__nav_items' >
           <i className={icon} /> 
           <span className='wrapper__nav__items__name' >{value}</span>
        </NavLink>
    )
}

export default Navigate