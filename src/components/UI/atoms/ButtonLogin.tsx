import { IBtn } from '../../../interface/interface';

export const ButtonLogin = ({text, link}: IBtn) => {
  return (
    <div className='wrapper__login__btn'>
      <a href={link} className='wrapper__login__btn__login'>{text}</a>
    </div>
  )
}

export default ButtonLogin