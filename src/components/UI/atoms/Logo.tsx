import { ILogo } from '../../../interface/interface';
const Logo = ({classFigure,classImage}: ILogo) => {
  return (
    <figure className={classFigure} >
      <img 
        className={classImage}
        src={require('../../../assets/images/spotify.svg').default} 
        alt='Logo' />
    </figure>
  )
}

export default Logo