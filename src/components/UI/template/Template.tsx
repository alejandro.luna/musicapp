import { ITemplate } from '../../../interface/interface'
import Sidebar from '../organisms/Sidebar';
import Body from '../organisms/Body';
import PageName from '../atoms/PageName';
import Songs from '../organisms/Songs';
;

const Template = ({namePage, dataSogs}: ITemplate) => {
  return (
    <>
        <Sidebar />
        <Body>
          <Songs dataSogs={dataSogs}/>
        </Body>
    </>
  )
}

export default Template