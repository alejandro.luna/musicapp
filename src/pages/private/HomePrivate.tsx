import Template from '../../components/UI/template/Template';
import { useSelector, RootStateOrAny } from 'react-redux';

const HomePrivate = () => {
  const {items} = useSelector((state: RootStateOrAny) => state.featurePlayList);
  return (
    <Template
      namePage='Home'
      dataSogs={items}
    />
  )
}

export default HomePrivate