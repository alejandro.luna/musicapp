import Template from "../../components/UI/template/Template"
import { useSelector, RootStateOrAny } from 'react-redux';

const FavoritosPrivate = () => {
  const {favoritesPlayList} = useSelector((state: RootStateOrAny) => state.favoritesPlayList);
  return (
    <Template
      namePage='Favorites'
      dataSogs={favoritesPlayList}
    />
  )
}

export default FavoritosPrivate