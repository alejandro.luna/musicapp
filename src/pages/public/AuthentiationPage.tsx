import LoginOrganisms from '../../components/UI/organisms/LoginOrganisms';

const AuthentiationPage = () => {
    return (
        <div className='wrapper__login'>
            <LoginOrganisms 
                classFigure='wrapper__login__figure'
                classImage='wrapper__img'
            />
        </div>
    )
}

export default AuthentiationPage