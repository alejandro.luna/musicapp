import { Navigate, Route, Routes } from 'react-router-dom';
import { routerPrivate } from './routers';
const AuthPrivateRouter = () => {
  return (
    <Routes>
      {
        routerPrivate.map(({path,Component}) => (
          <Route key={path} path={path} element={<Component/>} />
        ))
      }
      <Route path='/*' element={<Navigate to={routerPrivate[0].to} replace />} />
    </Routes>
  )
}

export default AuthPrivateRouter