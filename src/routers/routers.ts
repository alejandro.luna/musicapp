import {lazy, LazyExoticComponent} from 'react';

type JSXComponent = () => JSX.Element;

interface Router {
    to: string;
    path: string;
    className: string;
    icon: string;
    Component: LazyExoticComponent<JSXComponent> | JSXComponent;
    name: string;
}

const LazyAuthentication = lazy(() => import('../pages/public/AuthentiationPage'))

const LazyHome = lazy(() => import('../pages/private/HomePrivate'))
const LazyFavoritosPage = lazy(() => import('../pages/private/FavoritosPrivate'))

export const routerPublic: Router[] = [
    {
        path: '',
        to: '/',
        Component: LazyAuthentication,
        name: '',
        className: '',
        icon: ''
        },{
            path: 'authentication',
            to: '/authentication',
            Component: LazyAuthentication,
            name: 'Authentication',
            className: '',
            icon: ''
        }
    ]

export const routerPrivate: Router[] = [
    {
        path: '',
        to: '/',
        Component: LazyHome,
        name: '',
        className: '',
        icon: ''
    },{
        path: 'home',
        to: '/home',
        Component: LazyHome,
        name: 'Home',
        className: 'header_home',
        icon: 'icon fa fa-home'
    },{
        path: 'favorites',
        to: '/favorites',
        Component: LazyFavoritosPage,
        name: 'Favorites',
        className: 'header_fav',
        icon: 'icon fa fa-heart'
    }
] 