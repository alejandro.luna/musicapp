import { Suspense, useEffect } from 'react';
import { BrowserRouter } from 'react-router-dom'
import AuthPublicRouter from './AuthPublicRouter'
import AuthPrivateRouter from './AuthPrivateRouter';
import { getTokenFromURL, getAccessLocalStorage, setTokenLocalStorage } from '../helpers/token';
import { RootStateOrAny, useDispatch, useSelector } from 'react-redux';
import { authHash, authLoading, authLoaded, authLoginUser } from '../reducers/authReducer';
import { spotify } from '../services/spotify';
import { featureLoaded } from '../reducers/featurePlayListReducer';
import { sessionExpired } from '../helpers/auth';


const AppRouter = () => {
  const { isAuthenticated } = useSelector((state: RootStateOrAny) => state.auth)
  const dispatch = useDispatch()


  useEffect(() => {
    let time: any;
    const hash = getTokenFromURL()
    window.location.hash = ''

    if (hash.access_token !== '') {
      setTokenLocalStorage(hash)
    }
    if (getAccessLocalStorage('tk') !== '') {
      dispatch(authLoading())
      spotify.setAccessToken(getAccessLocalStorage('tk'));
      dispatch(authHash(getAccessLocalStorage('tk'), getAccessLocalStorage('expire_in'), getAccessLocalStorage('tk_type')));
      spotify.getMe().then(user => dispatch(authLoginUser(user)));
      spotify.getFeaturedPlaylists().then((songs) => {
        dispatch(featureLoaded(songs.body.playlists.items))
        dispatch(authLoaded())
      });

      time = setTimeout(() => {
        dispatch(sessionExpired())
      }, 3540000);

    }

    return () => { clearTimeout(time) }
  }, [dispatch])



  return (
    <Suspense fallback='Cargando...'>
      <BrowserRouter>
        {
          isAuthenticated
            ? <AuthPrivateRouter />
            : <AuthPublicRouter />
        }

      </BrowserRouter>
    </Suspense>
  )
}

export default AppRouter