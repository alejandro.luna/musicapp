import { Navigate, Route, Routes } from "react-router-dom"
import { routerPublic } from './routers';

const AuthPublicRouter = () => {
  return (
    <Routes>
      {
        routerPublic.map(({ path, Component }) => (
          <Route key={path} path={path} element={<Component />} />
        ))
      }
      <Route path='/*' element={<Navigate to={routerPublic[0].to} replace />} />
    </Routes>
  )
}

export default AuthPublicRouter