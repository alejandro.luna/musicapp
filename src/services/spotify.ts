import SpotifyWebApi from 'spotify-web-api-node'

export const spotify = new SpotifyWebApi({
    redirectUri: process.env.REACT_APP_REDIRECT_URI,
    clientId: process.env.REACT_APP_CLIENT_ID
});

const scopes: string[] =  ["user-read-currently-playing", "user-read-recently-played","user-read-playback-state","user-top-read","user-modify-playback-state"]


export const authorizeURL = `${process.env.REACT_APP_END_POINT}?client_id=${process.env.REACT_APP_CLIENT_ID}&response_type=token&redirect_uri=${process.env.REACT_APP_REDIRECT_URI}&scope=${scopes?.join("%20")}&show_dialog=true`