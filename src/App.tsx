import { Provider } from 'react-redux'
import AppRouter from './routers/AppRouter'
import { store } from './store';

const App = () => {

  return (
      <main className='wrapper'>
        <Provider store={store}>
          <AppRouter />
        </Provider>
      </main>
  )
}

export default App
