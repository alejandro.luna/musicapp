export const accessToken = {
    access_token: 'BQCSA9177CCxrgyvFhvGJTmFLdOWUTWvhUAY1Fl03-IpnXHwHjI41S1lLEl2IiUbK0b3Spo5iSh2YuZ8hyiHzvFzfKIZgzz-XrGO6nfUsehN4xGcPEbUO-GYqs8tIAhYI24EbWOwzChBtBBuHsVoBrLBr7Ly18L12SjKTHwvr_Aeja1FYIEj',
    token_type: 'Bearer',
    expires_in: '3600'
}

export const userData = {
    body: {
        display_name: 'Alejandro Luna Miranda',
        href: 'https://api.spotify.com/v1/users/31dqwjp4w44wxdthznnmxlihonqu',
        id: '31dqwjp4w44wxdthznnmxlihonqu',
        images: [],
        type: 'user',
        uri: 'spotify:user:31dqwjp4w44wxdthznnmxlihonqu',
        external_urls: { spotify: 'https://open.spotify.com/user/31dqwjp4w44wxdthznnmxlihonqu' },
        followers: { href: null, total: 0 }
    },
    headers: {
        cache_control: 'private, max-age=0',
        content_length: '233',
        content_type: 'application/json; charset=utf-8'
    },
    statusCode: 200
}

export const items = [
        {
            "collaborative": false,
            "description": "The very best in new music from around the world. Cover: Camila Cabello",
            "external_urls": {
                "spotify": "https://open.spotify.com/playlist/37i9dQZF1DWXJfnUiYjUKT"
            },
            "href": "https://api.spotify.com/v1/playlists/37i9dQZF1DWXJfnUiYjUKT",
            "id": "37i9dQZF1DWXJfnUiYjUKT",
            "isFavorite": false,
            "images": [
                {
                    "height": null,
                    "url": "https://i.scdn.co/image/ab67706f000000035fcdf80d7e62f9f86310b6bd",
                    "width": null
                }
            ],
            "name": "New Music Friday",
            "owner": {
                "display_name": "Spotify",
                "external_urls": {
                    "spotify": "https://open.spotify.com/user/spotify"
                },
                "href": "https://api.spotify.com/v1/users/spotify",
                "id": "spotify",
                "type": "user",
                "uri": "spotify:user:spotify"
            },
            "primary_color": null,
            "public": null,
            "snapshot_id": "MTY0NjM3MDAwMCwwMDAwMDM2MzAwMDAwMTdmNTM0YzhkNjUwMDAwMDE3ZjUyM2FkMmY0",
            "tracks": {
                "href": "https://api.spotify.com/v1/playlists/37i9dQZF1DWXJfnUiYjUKT/tracks",
                "total": 99
            },
            "type": "playlist",
            "uri": "spotify:playlist:37i9dQZF1DWXJfnUiYjUKT"
        },
        {
            "collaborative": false,
            "description": "New music from King Von, DaBaby and YoungBoy Never Broke Again.",
            "external_urls": {
                "spotify": "https://open.spotify.com/playlist/37i9dQZF1DX0XUsuxWHRQd"
            },
            "href": "https://api.spotify.com/v1/playlists/37i9dQZF1DX0XUsuxWHRQd",
            "id": "37i9dQZF1DX0XUsuxWHRQd",
            "isFavorite": false,
            "images": [
                {
                    "height": null,
                    "url": "https://i.scdn.co/image/ab67706f00000003eb717830dd59e8701029d3f2",
                    "width": null
                }
            ],
            "name": "RapCaviar",
            "owner": {
                "display_name": "Spotify",
                "external_urls": {
                    "spotify": "https://open.spotify.com/user/spotify"
                },
                "href": "https://api.spotify.com/v1/users/spotify",
                "id": "spotify",
                "type": "user",
                "uri": "spotify:user:spotify"
            },
            "primary_color": null,
            "public": null,
            "snapshot_id": "MTY0NjM3MDAwMCwwMDAwMDYzYzAwMDAwMTdmNTM0YzhkNzUwMDAwMDE3ZjUxM2ZiZTMx",
            "tracks": {
                "href": "https://api.spotify.com/v1/playlists/37i9dQZF1DX0XUsuxWHRQd/tracks",
                "total": 50
            },
            "type": "playlist",
            "uri": "spotify:playlist:37i9dQZF1DX0XUsuxWHRQd"
        }
    ]

    
export const songsItemsExpect: {} = {
    items: [
        {
            "collaborative": false,
            "description": "The very best in new music from around the world. Cover: Camila Cabello",
            "external_urls": {
                "spotify": "https://open.spotify.com/playlist/37i9dQZF1DWXJfnUiYjUKT"
            },
            "href": "https://api.spotify.com/v1/playlists/37i9dQZF1DWXJfnUiYjUKT",
            "id": "37i9dQZF1DWXJfnUiYjUKT",
            "isFavorite": true,
            "images": [
                {
                    "height": null,
                    "url": "https://i.scdn.co/image/ab67706f000000035fcdf80d7e62f9f86310b6bd",
                    "width": null
                }
            ],
            "name": "New Music Friday",
            "owner": {
                "display_name": "Spotify",
                "external_urls": {
                    "spotify": "https://open.spotify.com/user/spotify"
                },
                "href": "https://api.spotify.com/v1/users/spotify",
                "id": "spotify",
                "type": "user",
                "uri": "spotify:user:spotify"
            },
            "primary_color": null,
            "public": null,
            "snapshot_id": "MTY0NjM3MDAwMCwwMDAwMDM2MzAwMDAwMTdmNTM0YzhkNjUwMDAwMDE3ZjUyM2FkMmY0",
            "tracks": {
                "href": "https://api.spotify.com/v1/playlists/37i9dQZF1DWXJfnUiYjUKT/tracks",
                "total": 99
            },
            "type": "playlist",
            "uri": "spotify:playlist:37i9dQZF1DWXJfnUiYjUKT"
        },
        {
            "collaborative": false,
            "description": "New music from King Von, DaBaby and YoungBoy Never Broke Again.",
            "external_urls": {
                "spotify": "https://open.spotify.com/playlist/37i9dQZF1DX0XUsuxWHRQd"
            },
            "href": "https://api.spotify.com/v1/playlists/37i9dQZF1DX0XUsuxWHRQd",
            "id": "37i9dQZF1DX0XUsuxWHRQd",
            "isFavorite": false,
            "images": [
                {
                    "height": null,
                    "url": "https://i.scdn.co/image/ab67706f00000003eb717830dd59e8701029d3f2",
                    "width": null
                }
            ],
            "name": "RapCaviar",
            "owner": {
                "display_name": "Spotify",
                "external_urls": {
                    "spotify": "https://open.spotify.com/user/spotify"
                },
                "href": "https://api.spotify.com/v1/users/spotify",
                "id": "spotify",
                "type": "user",
                "uri": "spotify:user:spotify"
            },
            "primary_color": null,
            "public": null,
            "snapshot_id": "MTY0NjM3MDAwMCwwMDAwMDYzYzAwMDAwMTdmNTM0YzhkNzUwMDAwMDE3ZjUxM2ZiZTMx",
            "tracks": {
                "href": "https://api.spotify.com/v1/playlists/37i9dQZF1DX0XUsuxWHRQd/tracks",
                "total": 50
            },
            "type": "playlist",
            "uri": "spotify:playlist:37i9dQZF1DX0XUsuxWHRQd"
        }
    ]
}