import '@testing-library/jest-dom'
import { getTokenFromURL } from '../helpers/token';
import { ITk } from '../interface/interface';
import { accessToken } from './fixtures/data';

describe('Test Manipulación Access Token', () => { 
    test('Separar del hash el token en un array', () => { 
        const hash = '#access_token=BQCSA9177CCxrgyvFhvGJTmFLdOWUTWvhUAY1Fl03-IpnXHwHjI41S1lLEl2IiUbK0b3Spo5iSh2YuZ8hyiHzvFzfKIZgzz-XrGO6nfUsehN4xGcPEbUO-GYqs8tIAhYI24EbWOwzChBtBBuHsVoBrLBr7Ly18L12SjKTHwvr_Aeja1FYIEj&token_type=Bearer&expires_in=3600'
        const tokenExpected: ITk = {
            access_token: accessToken.access_token,
            token_type: accessToken.token_type,
            expires_in: accessToken.expires_in
        }
        window.location.hash = hash;
        const tokenFromUrl = getTokenFromURL()
        expect(tokenFromUrl).toEqual(tokenExpected)

    })
 })