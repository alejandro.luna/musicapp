import '@testing-library/jest-dom'
import SpotifyWebApi from 'spotify-web-api-node'
describe('Test Authenticatión', () => { 
    test('Login Test Spotify API', () => { 
        const credentials = {
            clientId: '5f28f217d15e4ac48a2f486649835da6',
            clientSecret: '81c48dcb28074bb4be02acac399c823f',
            redirectUri: 'http://192.168.1.54:3000',
        };
        const api = new SpotifyWebApi(credentials);
        expect(api.getCredentials().clientId).toBe(credentials.clientId);
        expect(api.getCredentials().clientSecret).toBe(credentials.clientSecret);
        expect(api.getCredentials().redirectUri).toBe(credentials.redirectUri);
     })
 })