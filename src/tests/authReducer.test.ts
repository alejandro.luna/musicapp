import '@testing-library/jest-dom'
import authReducer from '../reducers/authReducer';
import featurePlayListReducer from '../reducers/featurePlayListReducer';
import { IAction } from '../interface/interface';
import { accessToken, userData, items, songsItemsExpect } from './fixtures/data';

const demoAuth = {
    isAuthenticated: false,
    user: {},
    access: {},
    loading: false
}

describe('Test AuthReducer Spotify API', () => {
    /** LOADING AUTH */
    test('Loading Auth', () => { 
        const loadingAuth = {
            isAuthenticated: false,
            user: {}, access: {}, loading: true
        };
        const action: IAction = {
            type: 'AUTH_LOADING',
            payload: {loading: true}
        }
        const stateAuth = authReducer(loadingAuth, action)

        expect(loadingAuth).toEqual(stateAuth)
        
     });
    /** LOADED AUTH */
    test('Loaded Auth', () => { 
        const loadedAuth = {
            isAuthenticated: false,
            user: {}, access: {}, loading: false
        };
        const action: IAction = {
            type: 'AUTH_LOADED',
            payload: {loading: false}
        }
        const stateAuth = authReducer(loadedAuth, action)

        expect(loadedAuth).toEqual(stateAuth)
    });
    /** LOADED HASH AUTH **/
    test('Loaded Hash Auth', () => { 
        const cleanAuth = demoAuth;
        const action: IAction = {
            type: 'AUTH_HASH_LOGIN',
            payload: {
                access: {
                    token: accessToken.access_token,
                    expire_in: accessToken.expires_in,
                    token_type: accessToken.token_type
                }
            }
        }
        const stateAuth = authReducer(cleanAuth, action)
        expect(stateAuth).toEqual(demoAuth)
    });
    /* AUTH LOGIN */
    test('Login Auth', () => { 
        const loadingAuth = {
            isAuthenticated: false,
            user: {}, access: {}, loading: true
        };
        const action: IAction = {
            type: 'AUTH_LOADING',
            payload: {
                user : userData
            }
        }
        const stateAuth = authReducer(loadingAuth, action)

        expect(loadingAuth).toEqual(stateAuth)
        
     });
    /** LOGOUT AUTH */
    test('Logout Auth', () => { 
        const loadingAuth = demoAuth;
        const action: IAction = { type: 'AUTH_LOADING', payload: loadingAuth }
        const stateAuth = authReducer(loadingAuth, action);
        expect(loadingAuth).toEqual(stateAuth);        
    });
    /** LOGOUT FeaturePlayList */
    test('Loaded FeaturePlayList', () => { 
        const action: IAction = {
            type: 'FEATURE_LOADED',
            payload: {items}
        }
        const stateFeaturePlayList = featurePlayListReducer(items, action);
        expect(stateFeaturePlayList).toEqual(items);       
    });
 
 })