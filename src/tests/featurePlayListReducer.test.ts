import '@testing-library/jest-dom'
import authReducer from '../reducers/authReducer';
import featurePlayListReducer from '../reducers/featurePlayListReducer';
import { IAction } from '../interface/interface';
import { accessToken, userData, items, songsItemsExpect } from './fixtures/data';

const demoAuth = {
    isAuthenticated: false,
    user: {},
    access: {},
    loading: false
}

describe('Test AuthReducer Spotify API', () => {
    /** LOGOUT FeaturePlayList */
    test('Loaded FeaturePlayList', () => { 
        const action: IAction = {
            type: 'FEATURE_LOADED',
            payload: {items}
        }
        const stateFeaturePlayList = featurePlayListReducer(items, action);
        expect(stateFeaturePlayList).toEqual(items);       
    });
 
 })