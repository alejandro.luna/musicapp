import { ISong } from '../interface/interface';
import { featureSetIsFavorite } from '../reducers/featurePlayListReducer';
import { addFavoritesPlayList, deleteFavoriteSong } from '../reducers/favoritesReducer';
export const handleSetFavorite = ({dataSog}:ISong) => {
    return  async (dispatch: any) => {
        if (dataSog.isFavorite === undefined || dataSog.isFavorite === false) {
            dispatch(featureSetIsFavorite(dataSog.id,true))
            const {id, name, images, description} = dataSog
            dispatch(addFavoritesPlayList({id, name, description, images}))
        }else {
            dispatch(featureSetIsFavorite(dataSog.id, !dataSog.isFavorite))
            dispatch(deleteFavoriteSong(dataSog.id))
        }
    }
}