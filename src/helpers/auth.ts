import { authClean } from '../reducers/authReducer';
import { featureClean } from '../reducers/featurePlayListReducer';
import { favoritesClean } from '../reducers/favoritesReducer';

export const sessionExpired = () => {
    return async (dispatch: any) => {
        window.localStorage.removeItem('tk');
        window.localStorage.removeItem('expire_in');
        window.localStorage.removeItem('tk_type');
        dispatch(authClean());
        dispatch(featureClean());
        dispatch(favoritesClean());
    }
}