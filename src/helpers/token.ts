import { ITk } from '../interface/interface';

export const getTokenFromURL = () => {
    let responseAuth = window.location.hash.substring(1).split("&")
    let dataAuth: ITk = {'access_token':'' ,'token_type': '' , 'expires_in': ''}
    for (let i = 0; i < responseAuth.length; i++) {
        const element = responseAuth[i].split("=");
        for (let j = 0; j < element.length; j++) {
            if (element[0] === 'access_token') {
                dataAuth.access_token = element[1];
            } else if (element[0] === 'token_type') {
                dataAuth.token_type = element[1];
            } else if (element[0] === 'expires_in') {
                dataAuth.expires_in = element[1];
            }
        }   
    }
    return dataAuth
}

export const setTokenLocalStorage = (dataAuth: ITk): void =>{
    window.localStorage.setItem('tk', dataAuth.access_token)
    window.localStorage.setItem('expire_in', dataAuth.expires_in)
    window.localStorage.setItem('tk_type', dataAuth.token_type)
}

export const getAccessLocalStorage = (item: string): string => {
    let hash = window.localStorage.getItem(item) || '';
    return hash
}