export interface IPropsChildren { children: JSX.Element | any;}

export interface ILogo {
    classFigure: string,
    classImage: string
}

export interface IBtn {
    text: string;
    link?: string;
}

export interface IAction {
    type: string;
    payload?: boolean | string[] | {} | string | any;
}

export interface INavigate {
    to: string;
    value: string;
    className?: string;
    icon: string;
}

export interface ILabel { name: string; }

/* interace canciones */

export interface ITemplate {
    namePage: string;
    dataSogs: ISong[];
}
export interface ISongs {dataSogs: ISong[];}
export interface ISong {dataSog: IPlayListTop}

export interface IPlayListTop {
    description:   string;
    id:            string;
    images:        string[];
    name:          string;
    isFavorite?:    boolean | undefined ;
}

export interface ITk {
    access_token: string;
    token_type: string;
    expires_in: string;
}

export interface IImage {image:  IImages | string[] | string | any ;}

interface IImages {
    height: null;
    url:    string;
    width:  null;
}